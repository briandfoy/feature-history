package Feature::History;

=head1 NAME

Feature::History - version info about Perl features

=head1 SYNOPSIS

	use Feature::History;

	my $all_features = Feature::History->new;

	my $these_features = Feature::History->new( $^V );

	my $multi_features = Feature::History->new( @versions  );

	my @features = $all_features->list;

	my $feature = $features[0];

	$feature->type;
	$feature->name;
	$feature->description;


	# the first time this feature was stable
	# (some reverted to experimental)
	$feature->first_stable_version;

	# some features can be stable in disconnected versions if
	# they are experimental after they are stable, such as smart-match
	$feature->second_stable_version;

	# return versions
	$feature->first_experimental_version;
	$feature->first_deprecated_version;
	$feature->first_removed_version;

	# true if the feature is available in the running perl
	$feature->available;

	# true if the feature is stable in the running perl
	$feature->stable;

	# true if the feature is experimental in the running perl
	$feature->experimental;

	# true if the feature is deprecated in the running perl
	$feature->deprecated;

	# true if the feature is removed in the running perl
	# that is, the feature existed in earlier perls and is
	# not in the current one.
	$feature->removed;

	# true if the feature is that status in that version
	$feature->available_in( $version );
	$feature->stable_in( $version );
	$feature->experimental_in( $version );
	$feature->removed_in( $version );

=head1 DESCRIPTION

=over 4

=back

=head1 SOURCE AVAILABILITY

This module is on Github:

	https://github.com/briandfoy/feature-history

=head1 AUTHOR

brian d foy, C<< <bdfoy@cpan.org> >>

=head1 COPYRIGHT AND LICENSE

Copyright © 2015, brian d foy <bdfoy@cpan.org>. All rights reserved.
This software is available under the same terms as perl.

=cut
